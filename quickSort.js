/*
Write a Program that should contains 
1. Class Person with fields (name, age, salary, sex)
2. a static sort function in the class that should take array of Persons and name of the field and
 order of sorting and should return a new sorted array based on above inputs. Should not change intial array.

for example Person.sort(arr, 'name', 'asc') -> sort array of persons based on name in ascending order. 'desc' for descending

3. You have to write Quick sort for this sorting algorithms.
*/


class Person{
	constructor(name, age, salary, sex){
		this.name = name;
		this.age = age;
		this.salary = salary;
		this.sex = sex;
	}
	static partition(ar, left, right, key, order){
		let i = left-1;
		let pivot = ar[right][key];
		for(let j = left ; j < right; j++){

			if(order == 'asc' && ar[j][key] < pivot){
				i++;
				let tmp = ar[j];
				ar[j] = ar[i];
				ar[i] = tmp;
			}
			else if(order == 'desc' && ar[j][key] > pivot){
				i++;
				let tmp = ar[j];
				ar[j] = ar[i];
				ar[i] = tmp;
			}

		}
		let tmp = ar[++i];
		ar[i] = ar[right];
		ar[right] = tmp;
		return i;
	}
	static qSort(ar, left, right, key, order){
		if(left < right){
			let index = this.partition(ar, left, right, key, order);
			this.qSort(ar, left, index-1, key, order);
			this.qSort(ar, index+1, right, key, order);
		}
		return ar;
	}
	static sort(ar, key='salary', order = 'asc'){
		return this.qSort([...ar], 0, ar.length-1, key, order);

	}
}


// console.log(qSort([1,10,2,20,3,30], 0, 5));

let p1 = new Person("Avi", 32, 32000, "Male");
let p2 = new Person("Ravi", 12, 3000, "Male");
let p3 = new Person("Ravi", 22, 42000, "Male");
let p4 = new Person("Svi", 42, 12000, "Female");

let ar = [p1,p2,p3,p4]
Person.sort(ar).map(per=>console.log(per.salary));
console.log('---------')
Person.sort(ar, undefined, 'desc').map(per=>console.log(per.salary));