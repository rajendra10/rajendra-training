/*
Write a CRUD operation on cats using NodeJs and express only ( no databases)

CAT should have valid fields ( name, age, breed)

1. GET / -> List of cats
2. GET /:id -> Cat with given ID
3. PUT /:id -> Update cate with given ID with updated args
4. POST / -> Create CAT
5. DELETE /:id -> Delete Cat with given ID


You should handle all possible edge cases

*/

const express = require('express');
const app = express();
const DB = require("./db/index")

//
const okfy = (promise)=>{
	return new Promise((resolve, reject)=>{
		promise
		 .then(res=>resolve({ok: true,data : res }))
		 .catch(e=>reject({ok: false, data : res}))
	})
}

app.use(express.json());

let catDB = new DB('cat');

app.use((req, res, next)=>{
	res.setHeader("Content-Type", "application/json");
    next();
})

app.get('/', async(req, res)=>{

	let cats =  await okfy(catDB.get())
	return cats.ok 
				? res.status(200).send(cats)
				: res.status(500).send(cats)
})

app.get('/:id', async (req, res)=>{
	const {id} = req.params;
	let cats =  await okfy(catDB.get(id))

	return cats.ok 
				? res.status(200).send(cats)
				: res.status(500).send(cats)


})

app.post('/', async(req, res)=>{
	const cat = req.body;

	let result = await okfy(catDB.push(cat));
	return result.ok 
			? res.status(200).send(result)
			: res.status(500).send(result)


})

app.put('/:id', async(req, res)=>{
	const {id} = req.params;
	const obj = req.body;
	let result = await okfy(catDB.update(id, obj));
	return result.ok 
			? res.status(200).send(result)
			: res.status(500).send(result)

})

app.delete('/:id', async(req, res)=>{
	const {id} = req.params;
	let result = await (okfy(catDB.delete(id)));
	return result.ok 
			? res.status(200).send(result)
			: res.status(500).send(result)

})

app.listen(3030, ()=>{
	console.log('ACTIVE @PORT 3030');
})