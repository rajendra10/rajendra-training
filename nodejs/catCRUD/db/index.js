const fs = require('fs');
const path = require('path');

class DB{
	constructor(file,eraseOld = false){
		this.file = file+'.json';
		this.storePath = './store';
		this.createFile(eraseOld);
	}
//getters
	filePath(){
		return path.join(__dirname, this.storePath, this.file);
	}
	getFile(){
		try {
			let f = fs.readFileSync(this.filePath());
			f = JSON.parse(f);
			return f;
		}catch(e){
			return false;
		}
	}

//core
	createFile(eraseOld){
		try{
			fs.readFileSync(this.filePath())
			console.log('File already exists.')
			eraseOld && fs.writeFileSync(this.filePath(), JSON.stringify([]));//TODO TEST IT 

		}catch(e) {
			console.log('Creating new file');
			fs.writeFileSync(this.filePath(), JSON.stringify([]));
		}
	}
	updateFile(data){
		try{
			fs.writeFileSync(this.filePath(), data);
			return Promise.resolve({msg : "Updated DB"})
		}catch(e){
			return Promise.reject({msg : 'Failed to write to disk'});
		}
	}

//exposed 
	async get(val, key = 'id'){
		let data =  await this.getFile();
		if(!data) Promise.reject({msg: 'ERROR Reading file'});

		return !val ? data : data.filter(obj=>obj[key] == val); 

	}

	async push(obj){
		let data = await this.getFile();
		if(!data) return Promise.reject({msg : 'EROR Reading file'});

		data.push(obj);
		return this.updateFile(JSON.stringify(data));

	}

	async update(matchVal, updateObj, matchKey = 'id'){
		let data = await this.getFile();
		if(!data) return Promise.reject({msg : 'ERROR reading file'});
		data = data.map((obj)=>{
			if(obj[matchKey] == matchVal){
				console.log("matched", obj);
				return ({...obj, ...updateObj})
			}
			return  obj;
		})
		return this.updateFile(JSON.stringify(data));
	}

	async delete(matchVal, matchKey = 'id'){
		if(!matchVal) return Promise.reject({ok: false, msg: 'Must tell what to delete'});
		let data = await this.getFile();
		if(!data) Promise.reject({ok: false, msg : 'ERROR reading file'});

		data = data.filter(obj=>obj[matchKey] != matchVal);
		return this.updateFile(JSON.stringify(data));
	}
}



module.exports = DB;




//TEEST 


// let db = new DB('cat')
// db.delete(5).then(res=>{
// 	db.get().then(res=>{
// 		console.log(res);
// 	})
// })

// db.update(0, {name: 'NEWww NAME'}).then(res=>{
// 	console.log(res);

// 	db.get(5).then(a=>{
// 		console.log(a);
// 	})
// }).catch(e=>{
// 	console.log(e);
// })


// db.push({name : 'catty', breed : 'breedo', id : 5}).then(res=>{
// 	console.log(res);
// 	db.get(5).then((a,b)=>{
// 		console.log(a);
// 	})

// })


