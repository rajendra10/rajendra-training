//for loops
var letters = ['a','b','c','d','e'];

for(var i = 0; i < letters.length; i++){
	console.log(letters[i])
}

for(var letter of letters)
	console.log(letter);

for(var index in letters){
	console.log(letters[index]);
}




letters.forEach(letter=>console.log(letter));

letters.map(letter=>console.log(letter))



let data = {"employees":[  
    {"name":"Shyam", "email":"shyamjaiswal@gmail.com"},  
    {"name":"Bob", "email":"bob32@gmail.com"},  
    {"name":"Jai", "email":"jai87@gmail.com"}  
]} 


data.employees.map(emp=>{console.log(`${emp.name}, ${emp.email}`)})


