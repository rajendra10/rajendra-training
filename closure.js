//Closure Example source : mdn closure

function counter(start){
	let counterValue = start || 0;
	return {
		increment : ()=>counterValue++, 
		decrement : ()=>counterValue--,
		reset : ()=>counterValue = start || 0,
		value : ()=>counterValue,
	}
}

let counter1 = counter(10);
let counter2 = counter(100);

counter1.increment();
counter1.increment();
counter1.increment();
console.log("Counter1" , counter1.value())
counter1.decrement();
console.log("Counter1" , counter1.value())


counter2.increment();
counter2.increment();
console.log("Counter2" , counter2.value())
counter2.decrement();
console.log("Counter2" , counter2.value())

counter1.reset();
counter2.reset();
console.log('Resetting');
console.log("Counter1" , counter1.value())
console.log("Counter2" , counter2.value())



//adder 
function adder(val1){
	return (val2)=>val1+val2;
}

let add5 = adder(5);
let add100 = adder(100);

console.log(add5(10))
console.log(add100(3));


