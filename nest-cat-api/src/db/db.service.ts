import {Injectable} from '@nestjs/common'

import * as fs from 'fs'
import * as path from 'path';


@Injectable()
export class DBService{
	file : string;
	storePath :string;
	_constructor(file : string ,eraseOld : boolean = false){
		this.file = file+'.json';
		this.storePath = './';
		this.createFile(eraseOld);
	}
//getters
	filePath() : string{
		return path.join(__dirname, this.storePath, this.file);
	}
	getFile() : any{ // object | false
		try {
			let f : unknown = fs.readFileSync(this.filePath());
			f = JSON.parse(f as string);
			return f;
		}catch(e){
			return false;
		}
	}

//core
	createFile(eraseOld : boolean) : void{
		try{
			fs.readFileSync(this.filePath())
			console.log('File already exists.')
			eraseOld && fs.writeFileSync(this.filePath(), JSON.stringify([]));//TODO TEST IT 

		}catch(e) {
			console.log('Creating new file');
			fs.writeFileSync(this.filePath(), JSON.stringify([]));
		}
	}
	updateFile(data : string) : Promise<{msg:string}>{
		try{
			fs.writeFileSync(this.filePath(), data);
			return Promise.resolve({msg : "Updated DB"})
		}catch(e){
			return Promise.reject({msg : 'Failed to write to disk'});
		}
	}

//exposed 
	async get(val?, key :string= 'id'){
		let data =  await this.getFile();
		if(!data) Promise.reject({msg: 'ERROR Reading file'});

		return !val ? data : data.filter(obj=>obj[key] == val); 

	}

	async push(obj) : Promise<{msg:string}>{
		let data = await this.getFile();
		if(!data) return Promise.reject({msg : 'EROR Reading file'});

		data.push(obj);
		return this.updateFile(JSON.stringify(data));

	}

	async update(matchVal, updateObj, matchKey = 'id'){
		let data = await this.getFile();
		if(!data) return Promise.reject({msg : 'ERROR reading file'});
		data = data.map((obj)=>{
			if(obj[matchKey] == matchVal){
				console.log("matched", obj);
				return ({...obj, ...updateObj})
			}
			return  obj;
		})
		return this.updateFile(JSON.stringify(data));
	}

	async delete(matchVal, matchKey = 'id'){
		if(!matchVal) return Promise.reject({ok: false, msg: 'Must tell what to delete'});
		let data = await this.getFile();
		if(!data) Promise.reject({ok: false, msg : 'ERROR reading file'});

		data = data.filter(obj=>obj[matchKey] != matchVal);
		return this.updateFile(JSON.stringify(data));
	}
}





