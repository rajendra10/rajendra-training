import {Controller, Get, Post, Delete, Put, Body, Param} from '@nestjs/common';
import {CatsService} from './cats.service';
import {catDTO} from './dtos/dtos.cat'
@Controller('cats')
export class CatsController{
	constructor(private catsService : CatsService){}
	@Get(':id')
	getACat(@Param('id') id : number){
		return this.catsService.getCat(id);
	}

	@Get()
	getCats(){
		return this.catsService.getAllCats();
	}

	@Post()
	addCat(@Body() cat : catDTO){ //interface ? 
		return this.catsService.addCat(cat);
	}

	@Put(':id')
	updateCat(@Param('id') id : number, @Body() obj ){
		return this.catsService.updateCat(id, obj);
	}

	@Delete(':id')
	deleteCat(@Param('id') id : number){
		return this.catsService.deleteCat(id);
	}


}