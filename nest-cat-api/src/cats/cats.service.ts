import {Injectable} from '@nestjs/common'
import {DBService} from '../db/db.service';



interface Cat{
	name : string,
	breed : string,
	id: number
}


@Injectable()
export class CatsService{
	catDB : DBService
	constructor(){
		this.catDB = new DBService();
		this.catDB._constructor('cat');
	}
	getAllCats(){
		return this.catDB.get();
	}
	getCat(id:number){
		return this.catDB.get(id);
	}	
	addCat(cat:Cat){
		return this.catDB.push(cat);
	}
	updateCat(id:number, obj: Partial<Cat>){
		return this.catDB.update(id, obj);
	}
	deleteCat(id:number){
		return this.catDB.delete(id);
	}

}
