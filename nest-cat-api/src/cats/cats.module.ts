import {Module} from '@nestjs/common';
import {CatsController} from './cats.controller'
import {CatsService} from './cats.service'
import{DBService} from '../db/db.service';

@Module({
	providers : [CatsService, DBService],
	controllers: [CatsController]
})
export class CatsModule{}
