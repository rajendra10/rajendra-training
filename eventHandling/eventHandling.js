const btn = document.querySelector('#btn')
const text = document.querySelector('#text')


const clickHandler = ()=>{
	let clicked = 0;
	return ()=>text.textContent = `I have been Clicked ${++clicked} times`;
}

btn.addEventListener('click', clickHandler());





let body = document.querySelector('body');
let counter = document.querySelector('#counter');
const update = (val)=>counter.textContent = val;
const counterHandler = ()=>{
	let counter = 0;
	return (event)=>{
		switch(event.key){
			case 'ArrowRight' : 
			case 'ArrowUp' : update(++counter); break;
			case 'ArrowDown' : 
			case 'ArrowLeft' : update(--counter); break;
			default : console.log('Ignoring');
		}
	}
}

body.addEventListener('keydown', counterHandler());