//classes
//proto
//inheritance
//closure

function myArray(ar){
	this.store = ar || [];
	this.push = function(itm){
		return this.store.push(ar);
	}
	this.pop= function(){
	 return this.store.pop();
	}
	this.getItems = function(){
		return store;
	}
}

myArray.prototype.shuffle = function(){
	for(var i = 0; i < this.store.length; i++){
		var val1 = Math.floor(Math.random()* this.store.length);
		var val2 = Math.floor(Math.random()* this.store.length);
		let tmp = this.store[val1]
		this.store[val1] = this.store[val2];
		this.store[val2] = tmp;
	}
	return this.store;
}

var array = new myArray([1,2,3,4,5,6,7]);
var array2 = new myArray([1,2,3,4,5,6,7]);
// console.log(array.push(1));
// console.log(array.push(1));
// console.log(array.push(1));
// console.log(array.shuffle());

// console.log(array.__proto__ === myArray.prototype); // true
// console.log(array.push == array2.push); // false
// console.log(array.push === array2.push); // false
// console.log(array.shuffle === array2.shuffle); // true




/////////////////////////////////
// ES6 classes

class myArray2{
	constructor(ar){
		this.store = ar || []
	}

	get items(){
		return this.store;
	}
	push(itm){
		return this.store.push(itm);
	}

}



var array4 = new myArray2([1,2,3]);
var array5 = new myArray2([1,2,3,4,5])

// console.log(array4.push === array5.push);

array4.push(4);
// console.log(array4.items);


//TODO: Iterators and generators (mdn)



//Inheritance 

class Animal{
	constructor(name){
		this.name = name;
	}
	speak(){
		console.log(`${this.name}  make some kind of noise`);
	}
}

class Dog extends Animal{
	speak(){
		console.log(`${this.name}  Barks`);

	}
}

class Cat extends Animal{
	constructor(name){
		super(name);

	}
	speak(){
		console.log(`${this.name}  Mews`);
	}
}

let bunny = new Dog('Bunny');
bunny.speak();

let billo = new Cat('Billo');
billo.speak();