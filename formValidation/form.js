// Write a program that shows a form on web browser, with fields email, password, 
// sex (drop down with option of male, female, other), Role ( Radio button with option of Admin and User), 
// Permissions (Checkbox with option Perm1, Perm2, Perm3, Perm4), Submit button.

// On clicking Submit button, following validation should take place
// 1. All fields should be filled. 
// 2. Email should be valid
// 3. password should be min 6 character with MIX of Upercase, lowercase, digits
// 4. Atleast 2 permissions should be ticked.

// If all validation pass, the form and submit button should
//  disapper and all the filled in details in the form should appear with a Confirm Button.

const form = document.querySelector('form');
form.onsubmit = (e)=>{
	e.preventDefault();
	const form = document.querySelector('form');
	if(validateForm(form)){
		document.querySelector('input[name="submit"]').value = 'CONFIRM'
		//reset;
	}else{
		alert('FORM didn\'t pass the validation ' )
	}
}

var validateForm = (form)=>{
	if(isNull(form)) return false;
	if(notEmail(form.email.value)) return false;
	if(!checkPassword(form.password.value)) return false;
	return true;
}

var isNull = (form)=>{
	if( !form.name.value ||
		!form.email.value ||
		!form.password.value || 
		!form.sex.value ||
		!form.role.value ||
		form.querySelectorAll("input[name='permissions']:checked").length < 2
	   ) return true;

		return false
}

var notEmail = (email)=>!email.match('^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$')
var checkPassword = (password)=>{
	if(password.length < 6) return false;
	let lowerCase = false,
		upperCase = false,
		digit = false;

	for(let letter of password){
		if(letter > 'a' && letter < 'z') lowerCase = true;
		else if (letter > 'A' && letter <'Z') upperCase = true;
		else if (letter > '0' && letter < '9') digit = true;
	}

	return lowerCase && upperCase && digit;
}
