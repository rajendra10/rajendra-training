// ArrowFn ✓
// Destructuring

let [a, , b] = [1,2, 3] // a = 1, b = 3
let [c, ...d] = [1,2,3]  // a = 1 b = [2, 3]


let fun = ({userName: name})=>{
	console.log(name);
}
fun({userName: 'SomeName'});



let [w = 10] = [];  // destructuring with defaults



//map 
console.log([1,2,3,4].map(i=>i+1));

//filter 
let evens = new Array(10).fill(0).map((_,i)=>i).filter(i=>i%2==0);
console.log(evens);

let sumOfEvens = evens.reduce((acc, curVal)=>acc+curVal);
console.log(sumOfEvens);



//async 

let runAfterMs = (cb, ms)=>setTimeout(()=>cb(), ms)

// runAfterMs(()=>console.log('Hi after 3000'), 3000);



let delayer = (ms)=>{
	return new Promise((resolve, reject)=>{
		setTimeout(()=>resolve(),ms);
	})
}

(async function(){
	console.log('Inside Async Code');
	console.log('waiting 2s');
	await delayer(2000);
	console.log('2s passed');
}())

